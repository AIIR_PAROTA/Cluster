package pl.parota.cluster.util

import org.mongodb.scala.bson.BsonValue
import org.mongodb.scala.Document

import scala.collection.JavaConverters._

object MongoExtensions {

  implicit class BsonValueExtensions(bsonValue: BsonValue) {

    def toArray = bsonValue.asArray.asScala

    def toInt = bsonValue.asInt32.getValue

    def toText = bsonValue.asString.getValue

    def toDocument = Document(bsonValue.asDocument)
  }
}