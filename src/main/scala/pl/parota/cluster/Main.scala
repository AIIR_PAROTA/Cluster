package pl.parota.cluster

import java.util.concurrent.TimeUnit

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import pl.parota.cluster.model._
import pl.parota.cluster.model.heur.Association

import scala.collection.mutable.ArrayBuffer
import org.mongodb.scala._
import org.mongodb.scala.bson._
import org.mongodb.scala.model.Aggregates._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Projections._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Updates._
import org.mongodb.scala.model._
import pl.parota.cluster.model.feature._
import pl.parota.cluster.algorithm.heur._
import pl.parota.cluster.model.heur._
import pl.parota.cluster.model.heur.Constraints._
import pl.parota.cluster.model.heur.Weights._
import pl.parota.cluster.util.MongoExtensions._

import scala.concurrent.{Await, duration}
import scala.concurrent.duration.Duration
import scala.reflect.ClassTag
import scala.util.{Failure, Success}

object Main extends App {

  def newInstance = {
    val en0 = Entity(0, ArrayBuffer.empty, Nil, Map.empty)
    val en1 = Entity(1, ArrayBuffer.empty, Nil, Map.empty)
    val en2 = Entity(2, ArrayBuffer.empty, Nil, Map.empty)
    val en3 = Entity(3, ArrayBuffer.empty, Nil, Map.empty)
    val en4 = Entity(4, ArrayBuffer.empty, Nil, Map.empty)
    val en5 = Entity(5, ArrayBuffer.empty, Nil, Map.empty)
    val en6 = Entity(6, ArrayBuffer.empty, Nil, Map.empty)
    val en7 = Entity(7, ArrayBuffer.empty, Nil, Map.empty)

    val l0 = Location(0, ArrayBuffer.empty, Nil, Map.empty)
    val l1 = Location(1, ArrayBuffer.empty, Nil, Map.empty)
    val l2 = Location(2, ArrayBuffer.empty, Nil, Map.empty)
    val l3 = Location(3, ArrayBuffer.empty, Nil, Map.empty)

    val ts0 = TimeSlot(0, ArrayBuffer.empty, Nil, Map.empty)
    val ts1 = TimeSlot(1, ArrayBuffer.empty, Nil, Map.empty)
    val ts2 = TimeSlot(2, ArrayBuffer.empty, Nil, Map.empty)

    ts0.init(None, Some(ts1))
    ts1.init(Some(ts0), Some(ts2))
    ts2.init(Some(ts1), None)

    val tc = TimeContainer(0, ts0 :: ts1 :: ts2 :: Nil, Nil, Map.empty)

    val ev0 = Event(0, ArrayBuffer.empty, null, null, Nil, Map.empty)
    val ev1 = Event(1, ArrayBuffer.empty, null, null, Nil, Map.empty)
    val ev2 = Event(2, ArrayBuffer.empty, null, null, Nil, Map.empty)
    val ev3 = Event(3, ArrayBuffer.empty, null, null, Nil, Map.empty)
    val ev4 = Event(4, ArrayBuffer.empty, null, null, Nil, Map.empty)
    val ev5 = Event(5, ArrayBuffer.empty, null, null, Nil, Map.empty)
    val ev6 = Event(6, ArrayBuffer.empty, null, null, Nil, Map.empty)
    val ev7 = Event(7, ArrayBuffer.empty, null, null, Nil, Map.empty)
    val ev8 = Event(8, ArrayBuffer.empty, null, null, Nil, Map.empty)
    val ev9 = Event(9, ArrayBuffer.empty, null, null, Nil, Map.empty)

    SchedulingInstance(
      entities = en0 :: en1 :: en2 :: en3 :: en4 :: en5 :: en6 :: en7 :: Nil,
      locations = l0 :: l1 :: l2 :: l3 :: Nil,
      timeContainers = tc :: Nil,
      events = ev0 :: ev1 :: ev2 :: ev3 :: ev4 :: ev5 :: ev6 :: ev7 :: ev8 :: ev9 :: Nil,
      groups = Nil
    )
  }

  override def main(args: Array[String]) {
    val mongoClient = MongoClient(args(0)) // URI
    val mongoDB = mongoClient.getDatabase(args(1)) // DB Name
    val mongoCollection = mongoDB.getCollection[Document](args(2)) // Collection Name

    println("Reading from DB")

    val document = Await.result(mongoCollection
      .find(equal("_id", BsonObjectId(args(3))))
      .first.head, Duration(10, TimeUnit.SECONDS))

    println(document.toJson)

    val instance = SchedulingInstance(document("instance").toDocument)

    println("Calculating results")

    val assocs = for {
      ev <- instance.events filterNot isUnmanaged
      en <- instance.entities filterNot isUnmanaged
      l <- instance.locations filterNot isUnmanaged
      ts <- instance.timeContainers filterNot isUnmanaged flatMap { _.timeSlots } filterNot isUnmanaged
    } yield Association(ev, en, l, ts)

    val conf = new SparkConf().setAppName("App")
    implicit val sc = new SparkContext(conf)

    val baseConstraints = List(baseEntity _, baseEvent _, baseLocation _, baseTimeSlot _)
    val constraints = (document("instance").toDocument)("constraints").toArray map {
      c => parseConstraint((c.toDocument)("name").toText)
    }

    val weights = (document("instance").toDocument)("weights").toArray map {
      c => parseWeight((c.toDocument)("name").toText)
    }

    println("Constraints count: " + (baseConstraints ++ constraints).size)
    println("Weights count: " + weights.size)

    val assocsCount = assocs.size

    val result = scsp(assocs, instance, aggregator, regenerator, baseConstraints ++ constraints, weights)(p => {
      Await.result(mongoCollection
        .updateOne(equal("_id", BsonObjectId(args(3))), set("progress", (((assocsCount - p) / assocsCount.toDouble) * 100).toInt))
        .head, Duration(10, duration.SECONDS))
    })

    println("new")
    println(result.events mkString "\n")
    println("old")
    println(instance.events mkString "\n")

    println("Writing to DB")

    Await.result(mongoCollection
      .updateOne(equal("_id", BsonObjectId(args(3))), set("instance", result.toDocument))
      .head, Duration(10, duration.SECONDS))

    println("Success!")

//    val inputFile = args(0)
//    val outputFile = args(1)
//    val input = sc.textFile(inputFile)
//    val words = input.flatMap(line => line.split(" "))
//    val counts = words.map(word => (word, 1)).reduceByKey { case (x, y) => x + y }
//    counts.saveAsTextFile(outputFile)

//    val instance = newInstance
//
//    val assocs = for {
//      ev <- instance.events filterNot isUnmanaged
//      en <- instance.entities filterNot isUnmanaged
//      l <- instance.locations filterNot isUnmanaged
//      ts <- instance.timeContainers filterNot isUnmanaged flatMap { _.timeSlots } filterNot isUnmanaged
//    } yield Association(ev, en, l, ts)
//
//    scsp(assocs, instance, aggregator, List(entityBilocation _, locationBilocation _, baseEvent _), List(entityCount _))
//
//    println(instance.events.mkString("[\n\t", "\n\t", "\n]"))
  }
}
