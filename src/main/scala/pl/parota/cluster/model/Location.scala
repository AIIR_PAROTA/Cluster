package pl.parota.cluster.model

import org.mongodb.scala.Document
import pl.parota.cluster.model.constraint.{Constraint, ConstraintContainer}
import pl.parota.cluster.model.feature.{Feature, FeatureContainer, parseFeature}

import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConverters._

case class Location(id: Int,
                    events: ArrayBuffer[Event],
                    constraints: Seq[Constraint[Location]],
                    features: Map[String, Feature]) extends ConstraintContainer[Location] with FeatureContainer {

  override def toString: String = {
    val events = this.events map { _.id } mkString("[", ", ", "]")

    s"Entity($id) -> $events"
  }

  def toDocument: Document = {
    Document("id" -> id)
  }
}

object Location {

  def apply(document: Document): Location = {
    val id = document("id").asInt32.intValue
    val features = document("features").asArray.asScala
      .map(_.asString.getValue)
      .map(parseFeature _)
      .map(f => f.name -> f)
      .toMap

    Location(id, ArrayBuffer.empty, Nil, features)
  }
}