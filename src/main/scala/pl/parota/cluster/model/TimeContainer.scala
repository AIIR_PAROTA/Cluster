package pl.parota.cluster.model

import org.mongodb.scala.Document
import pl.parota.cluster.model.constraint.{Constraint, ConstraintContainer}
import pl.parota.cluster.model.feature.{Feature, FeatureContainer, parseFeature}

import scala.collection.JavaConverters._

case class TimeContainer(id: Int,
                         timeSlots: Seq[TimeSlot],
                         constraints: Seq[Constraint[TimeContainer]],
                         features: Map[String, Feature]) extends ConstraintContainer[TimeContainer] with FeatureContainer

object TimeContainer {

  def apply(document: Document): TimeContainer = {
    val id = document("id").asInt32.intValue
    val timeSlots = document("timeslots").asArray.asScala.toStream
      .map(bv => TimeSlot(Document(bv.asDocument)))
      .map(ts => ts.id -> ts)
      .toMap
    val features = document("features").asArray.asScala.toStream
      .map(_.asString.getValue)
      .map(parseFeature)
      .map(f => f.name -> f)
      .toMap

    document("timeslots").asArray.asScala
      .foreach { bv =>
        val id = bv.asDocument.get("id").asInt32.intValue
        val previous = Option(bv.asDocument.get("previous"))
          .map(_.asInt32.intValue)
          .map(timeSlots(_))
        val next = Option(bv.asDocument.get("next"))
          .map(_.asInt32.intValue)
          .map(timeSlots(_))

        timeSlots(id).init(previous, next)
      }

    TimeContainer(id, timeSlots.values.toSeq, Nil, features)
  }
}