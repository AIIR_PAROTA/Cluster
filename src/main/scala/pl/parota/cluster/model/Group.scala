package pl.parota.cluster.model

import org.mongodb.scala.Document
import pl.parota.cluster.model.constraint.{Constraint, ConstraintContainer}
import pl.parota.cluster.model.feature.{Feature, FeatureContainer, parseFeature}

import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConverters._

case class Group(id: Int,
                 constraints: Seq[Constraint[Entity]],
                 features: Map[String, Feature]) extends ConstraintContainer[Entity] with FeatureContainer

object Group {

  def apply(document: Document): Group = {
    val id = document("id").asInt32.intValue
    val features = document("features").asArray.asScala
      .map(_.asString.getValue)
      .map(parseFeature _)
      .map(f => f.name -> f)
      .toMap

    Group(id, Nil, features)
  }
}