package pl.parota.cluster.model

import org.mongodb.scala.Document
import pl.parota.cluster.model.constraint.{Constraint, ConstraintContainer}
import pl.parota.cluster.model.feature.{Feature, FeatureContainer, parseFeature}

import scala.collection.mutable.ArrayBuffer
import pl.parota.cluster.util.MongoExtensions._

case class Entity(id: Int,
                  events: ArrayBuffer[Event],
                  constraints: Seq[Constraint[Entity]],
                  features: Map[String, Feature]) extends ConstraintContainer[Entity] with FeatureContainer {

  private var _groups: Seq[Group] = Nil

  def groups = _groups

  def link(document: Document, group: Int => Group) = {
    _groups = document("groups").toArray
      .map(id => group(id.toInt))
  }

  override def toString: String = {
    val events = this.events map { _.id } mkString("[", ", ", "]")

    s"Entity($id) -> $events"
  }

  def toDocument: Document = {
    Document("id" -> id)
  }
}

object Entity {

  def apply(document: Document): Entity = {
    val id = document("id").toInt
    val features = document("features").toArray.toStream
      .map(_.toText)
      .map(parseFeature)
      .map(f => f.name -> f)
      .toMap

    Entity(id, ArrayBuffer.empty, Nil, features)
  }
}