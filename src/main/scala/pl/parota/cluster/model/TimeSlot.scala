package pl.parota.cluster.model

import org.mongodb.scala.Document
import pl.parota.cluster.model.constraint.{Constraint, ConstraintContainer}
import pl.parota.cluster.model.feature._

import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConverters._

case class TimeSlot(id: Int,
                    events: ArrayBuffer[Event],
                    constraints: Seq[Constraint[Entity]],
                    features: Map[String, Feature]) extends ConstraintContainer[Entity] with FeatureContainer {

  private var _previous = Option.empty[TimeSlot]
  private var _next = Option.empty[TimeSlot]

  def previous = _previous

  def next = _next

  def init(previous: Option[TimeSlot], next: Option[TimeSlot]) = {
    _previous = previous
    _next = next
  }

  override def toString: String = {
    val events = this.events map { _.id } mkString("[", ", ", "]")

    s"Entity($id) -> $events"
  }

  def toDocument: Document = {
    Document("id" -> id)
  }
}

object TimeSlot {

  def apply(document: Document): TimeSlot = {
    val id = document("id").asInt32.intValue
    val features = document("features").asArray.asScala
      .map(_.asString.getValue)
      .map(parseFeature)
      .map(f => f.name -> f)
      .toMap

    TimeSlot(id, ArrayBuffer.empty, Nil, features)
  }
}