package pl.parota.cluster.model

import org.mongodb.scala.Document
import pl.parota.cluster.model.constraint.{Constraint, ConstraintContainer}
import pl.parota.cluster.model.feature.{Feature, FeatureContainer, parseFeature}

import scala.collection.mutable.ArrayBuffer
import pl.parota.cluster.util.MongoExtensions._

case class Event(id: Int,
                 entities: ArrayBuffer[Entity],
                 var timeSlot: TimeSlot,
                 var location: Location,
                 constraints: Seq[Constraint[Event]],
                 features: Map[String, Feature]) extends ConstraintContainer[Event] with FeatureContainer {

  def link(document: Document, entity: Int => Entity, timeSlot: Int => TimeSlot, location: Int => Location) = {
    entities appendAll document("entities").toArray.map { id =>
      val en = entity(id.toInt)

      en.events append this

      en
    }

    this.timeSlot = Option(document("timeslot")) filter { _.toText != "" } map { id =>
      val ts = timeSlot(id.toText.toInt)

      ts.events append this

      ts
    } orNull

    this.location = Option(document("location")) filter { _.toText != "" } map { id =>
      val loc = location(id.toText.toInt)

      loc.events append this

      loc
    } orNull
  }

  override def toString: String = {
    val location = Option(this.location) map { _.id } map { _.toString } getOrElse("-")
    val timeSlot = Option(this.timeSlot) map { _.id } map { _.toString } getOrElse("-")
    val entities = this.entities map { _.id } mkString("[", ", ", "]")

    s"Event($id) in $location at $timeSlot with $entities"
  }

  def toDocument: Document = {
    Document(
      "id" -> id,
      "entities" -> entities.map(_.id),
      "timeslot" -> Option(timeSlot).map(_.id),
      "location" -> Option(location).map(_.id)
    )
  }
}

object Event {

  def apply(document: Document): Event = {
    val id = document("id").toInt
    val features = document("features").toArray.toStream
      .map(_.toText)
      .map(parseFeature)
      .map(f => f.name -> f)
      .toMap

    Event(id, ArrayBuffer.empty, null, null, Nil, features)
  }
}