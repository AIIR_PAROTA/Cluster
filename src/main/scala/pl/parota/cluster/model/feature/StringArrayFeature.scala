package pl.parota.cluster.model.feature


case class StringArrayFeature(name: String, value: Array[String]) extends ValueFeature[Array[String]]