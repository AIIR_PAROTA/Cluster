package pl.parota.cluster.model.feature

case class NumericFeature(name: String, value: Double) extends ValueFeature[Double]
