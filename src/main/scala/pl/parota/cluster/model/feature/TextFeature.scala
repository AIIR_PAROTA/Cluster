package pl.parota.cluster.model.feature

case class TextFeature(name: String, value: String) extends ValueFeature[String]