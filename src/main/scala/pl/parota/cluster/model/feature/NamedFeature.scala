package pl.parota.cluster.model.feature

case class NamedFeature(name: String) extends Feature
