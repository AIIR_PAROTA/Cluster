package pl.parota.cluster.model.feature

trait ValueFeature[V] extends Feature {

  val value: V
}
