package pl.parota.cluster.model.feature

trait FeatureContainer {

  val features: Map[String, Feature]
}
