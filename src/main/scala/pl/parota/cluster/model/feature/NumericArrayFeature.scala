package pl.parota.cluster.model.feature


case class NumericArrayFeature(name: String, value: Array[Double]) extends ValueFeature[Array[Double]]