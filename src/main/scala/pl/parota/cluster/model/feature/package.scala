package pl.parota.cluster.model

package object feature {

  val FEATURE_UNMANAGED = "unmanaged"
  val FEATURE_MIN_ENTITIES = "minEntities"
  val FEATURE_MAX_ENTITIES = "maxEntities"
  val FEATURE_PREFER_TIME_SLOT = "preferTimeSlot"
  val FEATURE_PREFER_LOCATION = "preferLocation"
  val FEATURE_WANTED_ENTITIES = "wantedEntities"
  val FEATURE_WANTED_LOCATIONS = "wantedLocation"
  val FEATURE_WANTED_TIME_SLOTS = "wantedTimeSlot"
  val FEATURE_MUST_ENTITIES = "mustEntities"
  val FEATURE_MUST_LOCATIONS = "mustLocation"
  val FEATURE_MUST_TIME_SLOTS = "mustTimeSlot"


  private val namedFeature = """(\w+)""".r
  private val numericFeature = """(\w+)=(\d*.?\d+)""".r
  private val textFeature = """(\w+)="(.*)"""".r
  private val numericArrayFeature = """(\w+)=\[((?:\d*.?\d+,\s*)*(?:\d*.?\d+))\]""".r
  private val stringArrayFeature = """(\w+)=\[((?:".*",\s*)*(?:".*"))\]""".r

  def parseFeature(raw: String) : Feature = raw match {
    case namedFeature(name) => NamedFeature(name)
    case numericFeature(name, value) => NumericFeature(name, value.toDouble)
    case textFeature(name, value) => TextFeature(name, value)
    case numericArrayFeature(name, values) => NumericArrayFeature(name, values.split("""\s*,\s*""").map(_.toDouble))
    case stringArrayFeature(name, values) => StringArrayFeature(name, values.split("""\s*,\s*""").map(_.stripPrefix("\"").stripSuffix("\"")))
    case _ => throw new IllegalArgumentException(raw + " is not a valid feature.")
  }

  def isUnmanaged(featureContainer: FeatureContainer) = featureContainer.features contains FEATURE_UNMANAGED
}
