package pl.parota.cluster.model.constraint

import pl.parota.cluster.model.Event
import pl.parota.cluster.model.feature.{Feature, FeatureContainer}

case class HardFeatureConstraint[C <: ConstraintContainer[C] with FeatureContainer](weight: Double, features: Seq[Feature]) extends WeightedHardConstraint[C] {
  def assert(container: C): Boolean = {
    features.forall(f => container.features.contains(f.name))
  }
}

case class EntityBilocation[Entity <: ConstraintContainer[Entity] with EntityBilocation[Entity]](weight: Double, events: Seq[Event]) extends WeightedHardConstraint[Entity]{
  def assert(container: Entity): Boolean = {
    container.events.groupBy(_.timeSlot).size == container.events.size
  }
}

case class LocationBilocation[Location <: ConstraintContainer[Location] with LocationBilocation[Location]](weight: Double, events: Seq[Event])  extends WeightedHardConstraint[Location]{
  def assert(container: Location): Boolean = {
    container.events.groupBy(_.timeSlot).size == container.events.size
  }
}

case class TimeWindows[Entity <: ConstraintContainer[Entity] with TimeWindows[Entity]](weight: Double, events: Seq[Event]) extends SoftConstraint[Entity]{
  def evaluate(container: Entity): Double = {
    weight * (container.events.size - container.events.count(event1 => container.events.exists(event2 => if(event2.timeSlot.next.isDefined) event1.timeSlot == event2.timeSlot.next.get else false)))
  }
}