package pl.parota.cluster.model.constraint

sealed trait Constraint[C <: ConstraintContainer[C]] {

  def evaluate(container: C): Double
}

trait SoftConstraint[C <: ConstraintContainer[C]] extends Constraint[C]

trait HardConstraint[C <: ConstraintContainer[C]] extends Constraint[C] {

  def assert(container: C): Boolean
}

trait WeightedHardConstraint[C <: ConstraintContainer[C]] extends HardConstraint[C] {
  val weight: Double
  def evaluate(container: C) = if (assert(container)) 0 else weight
}