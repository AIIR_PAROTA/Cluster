package pl.parota.cluster.model.constraint

trait ConstraintContainer[C <: ConstraintContainer[C]] {

  val constraints: Seq[Constraint[C]]
}
