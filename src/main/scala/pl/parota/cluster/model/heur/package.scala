package pl.parota.cluster.model

import pl.parota.cluster.algorithm.heur._

package object heur {

  val aggregator: Aggregator[Association, SchedulingInstance] = aggregate;

  def aggregate(solution: SchedulingInstance, association: Association): SchedulingInstance = {
    association.event.entities.append(association.entity)
    association.event.timeSlot = association.timeSlot
    association.event.location = association.location
    association.entity.events.append(association.event)
    association.location.events.append(association.event)
    association.timeSlot.events.append(association.event)

    return solution
  }

  def regenerator(solution: SchedulingInstance, association: Association): Association = {
    val event = solution.events.find{ _.id == association.event.id }.get
    val entity = solution.entities.find{ _.id == association.entity.id }.get
    val location = solution.locations.find{ _.id == association.location.id }.get
    val timeSlot = solution.timeContainers.flatMap(_.timeSlots).find{ _.id == association.timeSlot.id }.get

    return Association(event, entity, location, timeSlot)
  }
}
