package pl.parota.cluster.model.heur

import pl.parota.cluster.model.{Entity, Event, Location, TimeSlot}

case class Association(event: Event, entity: Entity, location: Location, timeSlot: TimeSlot) {

  override def toString: String = "Assoc(Ev(" + event.id + "), En(" + entity.id + "), Loc(" + location.id + "), Slot(" + timeSlot.id + "))"
}
