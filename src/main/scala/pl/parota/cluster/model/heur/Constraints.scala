package pl.parota.cluster.model.heur

import scala.collection.immutable.HashMap
import pl.parota.cluster.algorithm.heur._
import pl.parota.cluster.model.feature._

object Constraints {

  val parseConstraint: String => Constraint[Association] = HashMap[String, Constraint[Association]](
    "AlreadyAssigned" -> alreadyAssigned,
    "MaxEntities" -> maxEntities,
    "MustEntities" -> mustEntities,
    "MustLocations" -> mustLocations,
    "MustTimeSlots" -> mustTimeSlots
  )

  def alreadyAssigned(association: Association) = {
    !association.event.entities.exists(_.id == association.entity.id) &&
    association.event.location.id != association.location.id &&
    association.event.timeSlot.id != association.timeSlot.id
  }

  def baseEvent(association: Association) = {
    (association.event.location == null || association.event.location.id == association.location.id) &&
    (association.event.timeSlot == null || association.event.timeSlot.id == association.timeSlot.id)
  }

  def baseLocation(association: Association) = {
    !association.location.events.exists(e => e.timeSlot.id == association.timeSlot.id && e.id != association.event.id)
  }

  def baseEntity(association: Association) = {
    !association.entity.events.exists(_.timeSlot.id == association.timeSlot.id)
  }

  def baseTimeSlot(association: Association) = {
    true
  }

  def maxEntities(a: Association) = {
    val entitiesCount = a.event.entities.size

    a.event.features.get(FEATURE_MAX_ENTITIES).map(entitiesCount < _.asInstanceOf[NumericFeature].value.toInt).getOrElse(true) &&
      a.location.features.get(FEATURE_MAX_ENTITIES).map(entitiesCount < _.asInstanceOf[NumericFeature].value.toInt).getOrElse(true)
  }

  def mustEntities(a: Association) = {
    val features = a.event.features.get(FEATURE_MUST_ENTITIES) ++
      a.location.features.get(FEATURE_MUST_ENTITIES)

    features flatMap { _.asInstanceOf[StringArrayFeature].value } forall { a.entity.features.contains }
  }

  def mustLocations(a: Association) = {
    val features = a.event.features.get(FEATURE_MUST_LOCATIONS) ++
      a.entity.features.get(FEATURE_MUST_LOCATIONS)

    features flatMap { _.asInstanceOf[StringArrayFeature].value } forall { a.location.features.contains }
  }

  def mustTimeSlots(a: Association) = {
    val features = a.event.features.get(FEATURE_MUST_TIME_SLOTS) ++
      a.entity.features.get(FEATURE_MUST_TIME_SLOTS) ++
      a.location.features.get(FEATURE_MUST_TIME_SLOTS)

    features flatMap { _.asInstanceOf[StringArrayFeature].value } forall { a.timeSlot.features.contains }
  }
}
