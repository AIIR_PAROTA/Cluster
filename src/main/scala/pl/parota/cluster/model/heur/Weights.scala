package pl.parota.cluster.model.heur

import pl.parota.cluster.algorithm.heur._
import pl.parota.cluster.model.{Entity, Event, constraint, feature}
import pl.parota.cluster.model.constraint.HardFeatureConstraint
import pl.parota.cluster.model.feature._

import scala.collection.immutable.HashMap

object Weights {

  val parseWeight: Map[String, Weight[Association]] = HashMap[String, Weight[Association]](
    "NoWeight" -> noWeight,
    "EntityCount" -> entityCount,
    "MinEntities" -> minEntities,
    "PreferTimeSlots" -> preferTimeSlots,
    "PreferLocations" -> preferLocations,
    "WantedEntities" -> wantedEntities,
    "WantedLocations" -> wantedLocations,
    "WantedTimeSlots" -> wantedTimeSlots
  )

  def noWeight(association: Association) = 1.0

  def entityCount(a: Association): Double = -a.event.entities.size.toDouble

  def minEntities(a: Association): Double = {
    a.event.features.get(FEATURE_MIN_ENTITIES)
      .map(_.asInstanceOf[NumericFeature].value > a.event.entities.size.toDouble)
      .map( if(_) (1.0-(a.event.entities.size/a.event.features.get(FEATURE_MIN_ENTITIES).map(_.asInstanceOf[NumericFeature].value).sum))*2.0 else 0.0)
      .getOrElse(0.0)
  }

  def preferTimeSlots(a: Association): Double = {
    a.entity.features.get(FEATURE_PREFER_TIME_SLOT)
      .map(_.asInstanceOf[NumericArrayFeature].value.contains(a.timeSlot.id))
      .map( if(_) 2.0 else -2.0)
      .getOrElse(0.0) +
    a.event.features.get(FEATURE_PREFER_TIME_SLOT)
      .map(_.asInstanceOf[NumericArrayFeature].value.contains(a.timeSlot.id))
      .map( if(_) 2.0 else -2.0)
      .getOrElse(0.0) +
    a.location.features.get(FEATURE_PREFER_TIME_SLOT)
      .map(_.asInstanceOf[NumericArrayFeature].value.contains(a.timeSlot.id))
      .map( if(_) 2.0 else -2.0)
      .getOrElse(0.0)
  }

  def preferLocations(a: Association): Double = {
    a.entity.features.get(FEATURE_PREFER_LOCATION)
      .map(_.asInstanceOf[NumericArrayFeature].value.contains(a.location.id))
      .map( if(_) 2.0 else -2.0)
      .getOrElse(0.0) +
    a.event.features.get(FEATURE_PREFER_LOCATION)
      .map(_.asInstanceOf[NumericArrayFeature].value.contains(a.location.id))
      .map( if(_) 2.0 else -2.0)
      .getOrElse(0.0)
  }

  def wantedEntities(a: Association): Double = {
    a.event.features.get(FEATURE_WANTED_ENTITIES)
      .map(_.asInstanceOf[StringArrayFeature].value.count(a.entity.features.contains)).sum * 2.0 +
    a.location.features.get(FEATURE_WANTED_ENTITIES)
      .map(_.asInstanceOf[StringArrayFeature].value.count(a.entity.features.contains)).sum * 2.0
  }

  def wantedLocations(a: Association): Double = {
    a.event.features.get(FEATURE_WANTED_LOCATIONS)
      .map(_.asInstanceOf[StringArrayFeature].value.count(a.location.features.contains)).sum * 2.0 +
    a.entity.features.get(FEATURE_WANTED_LOCATIONS)
      .map(_.asInstanceOf[StringArrayFeature].value.count(a.location.features.contains)).sum * 2.0
  }

  def wantedTimeSlots(a: Association): Double = {
    a.event.features.get(FEATURE_WANTED_TIME_SLOTS)
      .map(_.asInstanceOf[StringArrayFeature].value.count(a.timeSlot.features.contains)).sum * 2.0 +
    a.entity.features.get(FEATURE_WANTED_TIME_SLOTS)
      .map(_.asInstanceOf[StringArrayFeature].value.count(a.timeSlot.features.contains)).sum * 2.0
  }

}
