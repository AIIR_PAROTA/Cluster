package pl.parota.cluster.model

import org.mongodb.scala._

import pl.parota.cluster.util.MongoExtensions._

case class SchedulingInstance(entities: Seq[Entity],
                              events: Seq[Event],
                              groups: Seq[Group],
                              locations: Seq[Location],
                              timeContainers: Seq[TimeContainer]) {

  def toDocument: Document = {
    Document(
      "events" -> events.map(_.toDocument),
      "entities" -> entities.map(_.toDocument),
      "timeslots" -> List.empty[Document],
      "timecontainers" -> List.empty[Document],
      "groups" -> List.empty[String],
      "locations" -> locations.map(_.toDocument)
    )
  }
}

object SchedulingInstance {

  def apply(document: Document): SchedulingInstance = {
    val entities = document("entities").toArray.toStream
      .map(_.toDocument)
      .map(d => (Entity(d), d))
      .map(en => en._1.id -> en)
      .toMap
    val events = document("events").toArray.toStream
      .map(_.toDocument)
      .map(d => (Event(d), d))
      .map(ev => ev._1.id -> ev)
      .toMap
    val groups = document("groups").toArray.toStream
      .map(_.toDocument)
      .map(Group(_))
      .map(g => g.id -> g)
      .toMap
    val locations = document("locations").toArray.toStream
      .map(_.toDocument)
      .map(Location(_))
      .map(l => l.id -> l)
      .toMap
    val timeContainers = document("timecontainers").toArray.toStream
      .map(_.toDocument)
      .map(TimeContainer(_))
      .map(tc => tc.id -> tc)
      .toMap
    val timeSlots = timeContainers.values
      .flatMap(_.timeSlots)
      .map(ts => ts.id -> ts)
      .toMap

    entities.values.foreach { case (en, d) =>
      en.link(d, groups)
    }

    events.values.foreach { case (ev, d) =>
      ev.link(d, entities(_)._1, timeSlots, locations)
    }

    SchedulingInstance(
      entities.values.map(_._1).toSeq,
      events.values.map(_._1).toSeq,
      groups.values.toSeq,
      locations.values.toSeq,
      timeContainers.values.toSeq
    )
  }
}