package pl.parota.cluster.algorithm

import org.apache.spark.SparkContext

import scala.reflect.ClassTag

package object heur {

  type Constraint[A] = A => Boolean
  type Weight[A] = A => Double
  type Aggregator[A, S] = (S, A) => S
  type Regenerator[A, S] = (S, A) => A

  def scsp[A: ClassTag, S](assocs: Seq[A], solution: S, aggregator: Aggregator[A, S], regenerator: Regenerator[A, S],
                           constraints: Seq[Constraint[A]], weights: Seq[Weight[A]])(progress: Int => Unit)(implicit sc: SparkContext): S = {
    val weighted = sc.parallelize(assocs)
      .filter(a => constraints forall { _(a) })
      .map(a => (a, weights.map{ _(a) }.sum))
      .collect

    progress(weighted.size)

    if (weighted isEmpty)
      solution
    else
      scsp(weighted.map(aw => regenerator(solution, aw._1)), aggregator(solution, regenerator(solution, weighted.maxBy(_._2)._1)),
        aggregator, regenerator, constraints, weights)(progress)
  }
}
