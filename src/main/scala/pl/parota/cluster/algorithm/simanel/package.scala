package pl.parota.cluster.algorithm

import org.apache.spark.SparkContext

package object simanel {

  type TemperatureFunc = Double => Double
  type ProbabilityFunc = (Double, Double) => Double

  def simulatedAnnealing(iterations: Int, temperature: TemperatureFunc, probability: ProbabilityFunc, solution: Solution)(implicit sc: SparkContext): Solution = {
    var currentSolution = solution
    var bestSolution = currentSolution

    for (i <- 0 until iterations) {
      val progress = i.toDouble / iterations
      val currentTemperature = temperature(progress)

      val neighbourhood = sc.
        parallelize(currentSolution.neighbourhood).
        map(s => (s, probability(currentTemperature, s.energy))).
        collect.
        scanRight((currentSolution, currentSolution.energy)) { case ((s0, p0), (s1, p1)) => (s1, p0 + p1) }
      val random = math.random * neighbourhood.head._2

      currentSolution = neighbourhood.
        filter({ case (s, p) => p > random }).
        last._1

      if (currentSolution.energy < bestSolution.energy)
        bestSolution = currentSolution
    }

    bestSolution
  }
}
