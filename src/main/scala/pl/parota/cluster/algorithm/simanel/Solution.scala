package pl.parota.cluster.algorithm.simanel

sealed trait Solution {

  def energy: Double

  def neighbourhood: Stream[Solution]

  def neighbourhoodSize: Int
}
