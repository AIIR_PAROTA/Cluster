# Cluster
1. Main goal
  * Implementation of algorithm , which solves problem of creating timetables.
2. Prerequisites
  * Apache Spark 2.1.0 (Prebuilt for Hadoop 2.7)
  * Scala 2.12.1
3. Configuration on Windows
  * For configuration on windows: [follow this tutorial](http://nishutayaltech.blogspot.in/2015/04/how-to-run-apache-spark-on-windows7-in.html) (Using Spark Prebuilt Package)
  * running master:
 
      `<spark_dir>/bin spark-class org.apache.deploy.master.Master` *check if master is running on localhost:8080*
  * running worker
  
      `<spark_dir>/bin spark-class org.apache.spark.deploy.worker.Worker spark://<ip>:<port>`   *\<ip\> - your ip address, \<port\> - usually 7077*
    * *check if worker is running on localhost:8081*
  * run spark-shell with your master
  
      `spark-shell --master spark://<ip>:<port>`
  * check if spark-shell is working properly
        * sample test in REPL:
      
      `val lines = sc.textFile("../README.md")`

      `lines.count()`
4. You could also use dockerized version (HARD-MODE, GODLIKE)
      
    
Dockerized version:
1. On remote host (do for every worker machine and the manager machine):
    * DISABLE FIREWALL!
    * `docker-machine create --driver virtualbox <whatever>`
    * `docker-machine stop <whatever>`
    * Go to Virtualbox -> <whatever> machine settings -> network. Enable 3rd network card. Set as brigde and tick "Connect cable".
    * `docker-machine start <whatever>`
    * Get assigned IP address: `docker-machine ssh <whatever> ifconfig -a` or when ip address couldn't be obtained assigne one by yourself (`ifconfig <IP_ADDRESS> netmask <MASK> up`)

2. On manager host (connection to remote hosts):
    * DISABLE FIREWALL!
    * `ssh-keygen -t rsa -b 2048` (you should generate it once)
    * `ssh-copy-id -i ~/.ssh/id_rsa.pub docker@<IP_ADDRESS>`
    * `docker-machine create --driver generic --generic-ip-address=<IP_ADDRESS> --generic-ssh-key ~/.ssh/id_rsa --generic-ssh-user docker <WORKER_NAME>`
    * `docker-machine regenerate-certs <WORKER_NAME>`
    * `docker-machine ssh <WORKER_NAME>`

3. Creating docker swarm:
    * on manager node execute command `docker swarm init --advertise-addr <MANAGER-IP>` and paste the result to each worker node
(on every worker node execute `docker swarm join --token=<result> <MANAGER-IP>:2377`)
    * Make sure that the swarm (all workers) initialized properly (execute on manager `docker node ls`)

4. Prepare spark configuration (on each node (workers and manager)):
    * Build your scala project via maven (`mvn clean package`)
    * Execute on manager node commands:
        - `cp -r /<MOUNT_LOCATION>/Cluster/spark_files .`
        - `cd spark_files`
        - `mkdir app && cp /<MOUNT_LOCATION>/Cluster/target/ClusterApp-1.0-SNAPSHOT.jar ./app` 
        - `docker network create --driver overlay --attachable spark-net`
        - `docker image pull t3g7/docker-spark`
    * Execute on every worker commands:
        - `scp -r docker@<MANAGER-IP>:/home/docker/spark_files .`
        - `docker image pull t3g7/docker-spark`
 
5. Launch spark:
    * On manager, in spark_files directory, execute `docker stack deploy --compose-file docker-compose.yml spark`
    * To scale your cluster execute `docker service scale spark_slave=x` (x>=2)
    * Excecute on manager node command `docker-compose -f proxy_compose.yml up -d`

6. To reset the cluster execute commands:
    * `docker stack rm spark`
    * follow "launch spark" steps.
